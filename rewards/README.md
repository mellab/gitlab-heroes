# Additional reward suggestions for Heroes

This page is currently a work in progress. Reference: https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/35

In order for GitLab Heroes to feel like they are true Heroes, their Heroic acts should be recognized in a certain way. Some of the ideas could include:
* Offer Badges for GitLab heroes as suggested by @haynes here https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/18. This could be for achieving a milestone, accomplishing a task, or mentoring new contributors.
* Adding an MVP (Most Valuable Person) section to the Heroes page to recognize a Hero who has made an outstanding contribution every month. 
* Highlighting major meetups that were a huge success and organized by one of the Heroes in the GitLab Hero circle.
* Sharing or some of the impactful content made by Heroes within the Gitlab Hero circle so as to inspire and teach other Heroes.
