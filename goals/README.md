# Goals/experience of the Heroes program

This page is currently a work in progress. Reference: https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/35

* Track and quantify contributions from Heroes so as to be in a better position to recognize and reward them better.
* Creating an open channel where both new and old contributors can seek help or give help or even coaching for new contributors. 
* Create a weekly or monthly webinar or coaching session on how to accomplish a particular task such as How to contribute to GitLab or How to organize a meetup. This could be open to being hosted by any of the Heroes as a way to share their experiences with other heroes and also bring the community together.  
* Help Heroes create content for GitLab. (inspired by @jamestharpe comment https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/35#note_286298517)
